<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Fact;
use Auth;
use Redirect;
use Session;

class FactsController extends Controller
{
    
    public function index()
    {   
        $facts = Fact::where('user_id','=',Auth::user()->id)->get();
        return view('admin.facts.index',compact('facts'));
    }

   
    public function create()
    {
       return view('admin.facts.create');
    }

    
    public function store(Request $request)
    {
         if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;

            Fact::create([
            'user_id'      =>Auth::user()->id,
            'title'        =>$request->title,
            'no_of_items'  =>$request->no_of_items,
            'img'          =>$request->img,

            ]);

            Session::flash('message', 'Successfully Added');
            return Redirect::back();

        }else{
             Fact::create([
            'user_id'      =>Auth::user()->id,
            'title'        =>$request->title,
            'no_of_items'  =>$request->no_of_items,
            ]);
            
            Session::flash('message', 'Successfully Added with out image');
            return Redirect::back();
        }
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $fact = Fact::find($id);
        return view('admin.facts.edit',compact('fact'));
    }

  
    public function update(Request $request, $id)
    {
        $facts = Fact::find($id);
        if (Input::hasFile('img')){
            $file = Input::file('img');
            $img_name = time()."-".$file->getClientOriginalName();
            $file->move('images',$img_name);
            $request->img = $img_name;


            $facts->title       = $request->title;
            $facts->no_of_items = $request->no_of_items;
            $facts->img         = $request->img;
            $facts->save();
           

            Session::flash('message', 'Successfully Updated');
            return Redirect::to('/facts');

        }else{

            $facts->title       = $request->title;
            $facts->no_of_items = $request->no_of_items;
            $facts->save();
            Session::flash('message', 'Successfully Updated');
            return Redirect::to('/facts');
        }
    }

   
    public function destroy($id)
    {
        $facts = Fact::destroy($id);
         
        Session::flash('message','Successfully Deleted');
         return Redirect::to('/facts');
    }
}
