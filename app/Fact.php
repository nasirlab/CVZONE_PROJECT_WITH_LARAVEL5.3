<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Fact extends Model
{
	use SoftDeletes;
   protected $fillable=[

       'title','user_id','no_of_items','img',
       
   ];

   protected $dates = [ ' deleted_at'];

}
