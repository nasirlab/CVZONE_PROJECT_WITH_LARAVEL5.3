 <div id="my-panel">
        </div>
        <!-- PLUGIN SCRIPTS -->
        <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/default.js') }}"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="{{ asset('/js/watch.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/layout.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/main.js') }}"></script>
        <!-- END PLUGIN SCRIPTS -->
</body>

</html>
