<article class="hs-content works-section" id="section7">
    <span class="sec-icon fa fa-archive"></span>
    <div class="hs-inner">
        <span class="before-title">.07</span>
        <h2>WORKS</h2>
        <div class="portfolio">
            <!-- Portfolio Item -->
            @php
                $i = 0;
            @endphp
            @foreach ($allData['portfolios'] as $portfoliosData) 
                 @php
                      $i++;
                 @endphp
                <figure class="effect-milo">
                    <img src="{{ asset('/images').'/'.$portfoliosData->img }}" alt="img11" width="282" height="222" />
                    <figcaption>
                        <span class="label">{{ $portfoliosData->category }}</span>
                        <div class="portfolio_button">
                            <h3>{{ $portfoliosData->title }}</h3>
                            <a href=".work{{ $i }}" class="open_popup" data-effect="mfp-zoom-out">
                                <i class="hovicon effect-9 sub-b"><i class="fa fa-search"></i></i>
                            </a>
                        </div>
                        <div class="mfp-hide mfp-with-anim work_desc work{{ $i }}">
                            <div class="col-md-6">
                                <div class="image_work">
                                    <img src="{{ asset('/images').'/'.$portfoliosData->img }}" alt="img" width="560" height="420">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="project_content">
                                    <h2 class="project_title">{{ $portfoliosData->title }}</h2>
                                    <p class="project_desc">
                                        {{ $portfoliosData->description }}
                                </div>
                            </div>
                            <a class="ext_link" href="#"><i class="fa fa-external-link"></i></a>
                            <div style="clear:both"></div>
                        </div>
                    </figcaption>
                </figure>              
            @endforeach
            <!-- End Portfolio Item -->
        </div>
        <!-- End Portfolio Wrapper -->
    </div>
</article>