@extends('admin.layouts.master')
@section('posts_menu_add','active')
@section('pageTitle')
<span class="text-semibold">POSTS - ADD</span>  || <a href="/posts">MY POSTS</a>
@endsection

@section('content')
	<div class="row">
	
			 {!! Form::open(['url'=>'/posts','method'=>'POST','files'=>'true']) !!}
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<div class="col-md-5">
								@if(Session::has('message'))
									<div class="alert alert-info" >
										{{ Session::get('message') }}
									</div>
								@endif
									<div class="form-group">

										{!! Form::label('title','Posts Title') !!}
										{!! Form::text('title',null,['placeholder'=>'Learn PHP','class'=>'form-control']) !!}
										
									</div>	
									<div class="form-group">
										{!! Form::label('author_name','Author name') !!}
										{!! Form::text('author_name',null,['placeholder'=>'Rahim','class'=>'form-control']) !!}
										
									</div>												
									<div class="form-group">
										{!! Form::label('categories','Categoty name') !!}
										{!! Form::text('categories',null,['placeholder'=>'Education','class'=>'form-control']) !!}
										
									</div>										
									
									<div class="form-group">
										{!! Form::label('description','Description') !!}
										{!! Form::textarea('description',null,['class'=>'form-control']) !!}
										
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										{!! Form::label('country_name','Country Name') !!}
										{!! Form::text('country_name',null,['placeholder'=>'Bangladesh','class'=>'form-control']) !!}
									</div>					
									<div class="form-group">
										{!! Form::label('city_name','City Name') !!}
										{!! Form::text('city_name',null,['placeholder'=>'Dhaka','class'=>'form-control']) !!}
									</div>										
									<div class="form-group">
										{!! Form::label('tags','Tags') !!}
										{!! Form::text('tags',null,['placeholder'=>'PHP','class'=>'form-control']) !!}
									</div>										
									<div class="form-group">
										{!! Form::label('img','Post Image') !!}
										{!! Form::file('img',['class'=>'form-control']) !!}
									</div>					
								</div>
							</div>
                            
							<div class="form-group">
								{!! Form::submit('Save') !!}
							</div>
						</div>
					</div>
				</fieldset>
			{!! Form::close() !!}
	</div> 	
@endsection