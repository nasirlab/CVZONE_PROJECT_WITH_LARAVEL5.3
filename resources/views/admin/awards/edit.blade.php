@extends('admin.layouts.master')
@section('awards_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">AWARDS - EDIT</span>  || <a href="/awards">MY AWARDS</a> || <a href="/awards/create">ADD NEW</a>
@endsection

@section('content')
<div class="row ">
    <!-- about basic info about module -->
	{!! Form::open(['url'=>['/awards',$award->id],'method'=>'PUT']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
					<div class="row">
							<h5>Edit your awards .</h5>
						<!-- section one -->
						<div class="col-md-5">
							<div class="form-group">
								{!! Form::label('title','Awards Title') !!}
								{!! Form::text('title', $award->title ,['placeholder'=>'New Programmer','class'=>'form-control']) !!}
							</div>			
							<div class="form-group">
								{!! Form::label('year','Awards Year') !!}
								{!! Form::text('year', $award->year ,['placeholder'=>' 2001','id'=>'datepicker','class'=>'form-control']) !!}
							</div>										
							<div class="form-group">
								{!! Form::label('description','Sort description') !!}
								{!! Form::textarea('description', $award->description ,['class'=>'form-control']) !!}
							</div>									
						</div>								
						<!-- Second section -->							
						<div class="col-md-5">
							<div class="form-group">
								{!! Form::label('organization','Organaizations Name') !!}
								{!! Form::text('organization', $award->organization ,['placeholder'=>' Webtech','class'=>'form-control']) !!}
							</div>					
							<div class="form-group">
								{!! Form::label('location','Organaization Location') !!}
								{!! Form::text('location', $award->location ,['placeholder'=>' Bangladesh','class'=>'form-control']) !!}
							
							</div>					
						</div>
					</div>
					<div class="form-group">
						{!! Form::submit('Update',['class'=>'marg-top']) !!}
					</div>
				</div>
			</div>
		</fieldset>
	{!! Form::close() !!}
</div>
@endsection