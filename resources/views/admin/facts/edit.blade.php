@extends('admin.layouts.master')
@section('facts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">FACTS - EDIT</span>  || <a href="/facts"> MY FACTS</a> || <a href="/facts/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row" >
	 {!! Form::open(['url'=>['/facts',$fact->id],'method'=>'PUT','files'=>'true']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
						<h5>You can edit your facts .</h5>
							<div class="form-group">
								{!! Form::label('title','Facts Title') !!}
								{!! Form::text('title',$fact->title,['placeholder'=>'Ex. web design','class'=>'form-control']) !!}	
							</div>															
							<div class="form-group">
								{!! Form::label('no_of_items','Facts numbers') !!}
								{!! Form::text('no_of_items',$fact->no_of_items,['placeholder'=>'Ex. 50','class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('img','Facts image') !!}
								{!! Form::file('img',['class'=>'form-control']) !!}
								<img width="90px" height="70" src="{!! asset('images').'/'.$fact->img !!}" alt="No Image">
							</div>
							<div class="form-group">
								{!! Form::submit('Update') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
       {!! Form::close() !!}
</div>
@endsection