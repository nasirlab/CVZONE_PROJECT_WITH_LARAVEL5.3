@extends('admin.layouts.master')
@section('facts_menu_add','active')
@section('pageTitle')
<span class="text-semibold">FACTS - ADD</span>  || <a href="/facts"> MY FACTS</a>
@endsection

@section('content')
<div class="row">

	  {!! Form::open(['url'=>'/facts','method'=>'POST','files'=>'true']) !!}
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
							@if(Session::has('message'))
								<div class="alert alert-info" >
									{{ Session::get('message') }}
								</div>
							@else
								<h5>Please add your facts here .</h5>
							@endif
						
							<div class="form-group">
								{!! Form::label('title','Facts Title') !!}
								{!! Form::text('title',null,['placeholder'=>'Ex. web design','class'=>'form-control']) !!}	
							</div>	
							<div class="form-group">
								{!! Form::label('img','Facts image') !!}
								{!! Form::file('img',['class'=>'form-control']) !!}
							</div>														
							<div class="form-group">
								{!! Form::label('no_of_items','Facts numbers') !!}
								{!! Form::text('no_of_items',null,['placeholder'=>'Ex. 50','class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::submit('Save') !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
       {!! Form::close() !!}
</div>	
@endsection